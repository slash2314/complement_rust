extern crate image;
extern crate time;

use std::io::prelude::*;
use std::fs::File;
use std::path::Path;
use std::string::*;
use std::process;
use std::io::ErrorKind;
use std::io;
use image::{
    ImageBuffer,
};
use std::env;

//#[derive(Debug)]
//struct Point {
//    x: i32,
//    y: i32
//}
//
//impl Point {
//    fn new(x: i32, y: i32) -> Point {
//        Point { x: x, y: y }
//    }
//}

fn is_complement(a: &char, b: &char) -> bool {
    return match (*a, *b) {
        ('G', 'C') | ('C', 'G') | ('A', 'T') | ('T', 'A') => true,
        _ => false
    }
}

fn write_image(f: &mut File, img: ImageBuffer<image::Luma<u8>, std::vec::Vec<u8>>) -> Result<(), image::ImageError> {
    let res = image::ImageLuma8(img).save(f, image::PNG)?;
    Ok(res)
}

fn get_fasta<P: AsRef<Path>>(path: P) -> Result<Vec<char>, io::Error> {
    let path_ref: &Path = path.as_ref();
    let file_name = path_ref.display();
    let mut f = File::open(path_ref)?;
    let meta_data = f.metadata()?;
    if meta_data.is_dir() {
        return Err(io::Error::new(ErrorKind::Other, format!("{} is a directory", file_name)));
    }
    let mut s = String::new();
    f.read_to_string(&mut s)?;
    let lines: Vec<String> = s.as_str().lines()
        .map(|s| s.to_string())
        .skip(1)
        .collect();
    Ok(lines.join("").chars().collect())
}

fn main() {
    let file_name = match env::args().nth(1) {
        Some(filename) => filename,
        None => {
            println!("Couldn't get filename argument.");
            process::exit(1);
        }
    };
    let ref mut image_file = match File::create("complement.png") {
        Ok(image_file) => { image_file }
        Err(e) => {
            println!("Problem creating image file; {}", e);
            process::exit(1);
        }
    };
    let path = Path::new(&file_name);
    println!("Using filename: {}", path.display());
    let seq = match get_fasta(path) {
        Ok(seq) => seq,
        Err(e) => {
            println!("Couldn't get fasta: {}", e);
            process::exit(1);
        }
    };
    println!("seq size {} bytes", seq.len());
    let seq_len = seq.len() as u32;
    let mut count = 0u64;
    let mut point_count = 0u64;
    let mut img = ImageBuffer::new(seq_len, seq_len);
    for r in 0..seq_len {
        for c in 0..seq_len {
            if seq[r as usize] == seq[c as usize] {//is_complement(&seq[r as usize], &seq[c as usize]) {
                img.put_pixel(r, c, image::Luma([255u8]));
                point_count+=1;
            }
            count += 1;
        }
    }
    println!("number of pairs: {}", count);
    println!("Number of points: {}", point_count);
    println!("{:.2}% are complements", (point_count as f64) / (count as f64) * 100.0);
    match write_image(image_file, img) {
        Err(e) => {
            println!("problem with writing image: {}", e);
        }
        _ => {}
    }
}
